<?php

namespace Anna\Php2\App;

abstract class Model
{
    public const TABLE = '';

    public $id;

    /**
     * возвращает все записи в таблице
     * @return array|bool
     */
    public static function findAll()
    {
        $db = new Db();
        return $db->query("SELECT * FROM " . static::TABLE);
    }

    /**
     * возвращает одну запись из таблицы данной модели с указанным $id
     * @param int $id
     * @return array|bool
     */
    public static function findById($id)
    {
        $db = new Db();
        return $db->query(
            "SELECT * FROM " . static::TABLE . ' WHERE id =:id',
            ['id' => $id],
            static::class);
    }
}