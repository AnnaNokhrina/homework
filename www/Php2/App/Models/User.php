<?php

namespace Anna\Php2\App\Models;

use Anna\Php2\App\Model;

class User extends Model
{
    public const TABLE = 'user';

    public $email;
    public $name;
}