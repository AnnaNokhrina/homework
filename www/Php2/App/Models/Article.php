<?php

namespace Anna\Php2\App\Models;

use Anna\Php2\App\Db;
use Anna\Php2\App\Model;

class Article extends Model
{
    public const TABLE = 'news';

    public $title;
    public $content;

    /**
     * выполняет запрос с параметрами $paramsSql (например, ...order by column_name limit 100;)
     * возвращает массив результата запроса
     * @param string $paramsSql
     * @return array|bool
     */
    public function findBySql($paramsSql)
    {
        $sql = "SELECT * FROM " . self::TABLE . ' ' . $paramsSql;
        $db = new Db();
        return $db->query($sql, [], self::class);
    }
}