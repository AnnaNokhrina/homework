<?php

namespace Anna\Php2\App;
use PDO;
require_once __DIR__ . '/../autoload.php';

class Db
{
    /** @var PDO $dbh обработчик бд */
    protected $dbh;

    /** Db constructor, выполняет соединение между php и бд */
    public function __construct()
    {
        $config = require __DIR__ . '/../config_db.php';
        $this->dbh = new PDO($config['db']['dsn'], $config['db']['username'], $config['db']['password'], $config['db']['options']);
    }

    /**
     * возвращает отформатированную строку $sql запроса с параметрами $data[], готовую к выполнению
     * @param string $sql
     * @param array $data
     * @return string
     */
    public function format($sql, $data)
    {
        $fields = [];
        foreach ($data as $key => $value) {
            $fields[':' . $key] = $value;
        }

        foreach ($fields as $key => $value) {
            $sql = str_replace($key, $value, $sql);
        }
        return $sql;
    }

    /**
     * выполняет запрос $sql с параметрами $params[]
     * возвращает массив $data[] объектов класса $className
     *
     * @param string $sql
     * @param array $params
     * @param string $className
     * @return array|bool
     */
    public function query($sql, $params = [], $className)
    {
        $query = $this->format($sql, $params);
        $data = $this->dbh->query($query)->fetchAll(PDO::FETCH_CLASS, $className);
        if ($data) {
            return $data;
        }
        return false;
    }

    /**
     * проверяет выполнение запроса $query с параметрами $params[]
     * для запросов, не возвращающих данные (insert, update, delete...)
     *
     * @param string $query
     * @param array $params
     * @return bool
     */
    public function execute($query, $params = [])
    {
        $formatSql = $this->format($query, $params);
        return $this->dbh->prepare($formatSql)->execute();
    }
}