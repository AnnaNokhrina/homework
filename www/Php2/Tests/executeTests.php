<?php

require_once __DIR__ . '/../autoload.php';
ini_set('display_errors', 1);

$db = new \Anna\Php2\App\Db();

$tests = [
    1 => [
        'query' => "INSERT INTO news (`title`, `content`) VALUES (':title', ':content')",
        'params' => ['title' => 'title', 'content' => 'content']
    ],
    2 => [
        'query' => "UPDATE news SET `title` = ':title', `content` = ':content' WHERE id = :id",
        'params' => ['id' => 50, 'title' => 'title1', 'content' => 'content1']
    ],
    3 => [
        'query' => "DELETE FROM news WHERE id = :id",
        'params' => ['id' => 67]
    ]
];

function myAssert($test)
{
    return true == $test;
}

foreach ($tests as $key => $test) {
    var_dump(myAssert($db->execute($tests[$key]['query'], $tests[$key]['params'])));
}
