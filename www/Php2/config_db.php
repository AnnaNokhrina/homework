<?php

return [
    "db" => [
        "dsn" => "mysql:host=mysql_db;dbname=php2",
        "options" => array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'),
        "username" => "root",
        "password" => "root"
    ]
];