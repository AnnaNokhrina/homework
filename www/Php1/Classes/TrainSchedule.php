<?php

namespace Anna\MyCity\Php\Classes;
require_once __DIR__ . '/../autoload.php';

class TrainSchedule
{
    /**
     * @return array, расписание всех поездов
     */
    public function getTrains()
    {
        $db = Db::init();
        $db->query("SELECT * FROM train_schedule");
        return $db->fetchAll();
    }

    /**
     * обновляет расписание выбранного поезда в соответствие с $data
     * @param array $data
     * @return bool
     */
    public function edit($data)
    {
        $db = Db::init();
        $fields = $db->format($data);

        return $db->query("
            UPDATE train_schedule 
            SET `train` = ':train', `route` = ':route', `departure` = ':departure', `arrival` = ':arrival', `time` = ':time', `price` = ':price' 
            WHERE id=:id", $fields);
    }

    /**
     * добавляет расписание поезда $data
     * @param array $data
     * @return bool
     */
    public function add($data)
    {
        $db = Db::init();
        $fields = $db->format($data);

        return $db->query("
            INSERT INTO train_schedule (`train`, `route`, `departure`, `arrival`, `time`, `price`) 
            VALUES (':train', ':route', ':departure', ':arrival', ':time', ':price')
        ", $fields);
    }

    /**
     * возвращает расписание поезда
     * @param int $id
     * @return bool|array
     */
    public function getTrain($id)
    {
        $db = Db::init();
        $db->query("SELECT * FROM train_schedule WHERE id = ':id'", [':id' => $id]);
        return $db->fetch();
    }

    /**
     * удаляет выбранное расписание поезда
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        $db = Db::init();
        return $db->query("DELETE FROM train_schedule WHERE id=:id", [':id' => $id]);
    }
}