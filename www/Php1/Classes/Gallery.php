<?php

namespace Anna\MyCity\Php\Classes;

class Gallery
{
    /**
     * @var string
     */
    protected $path = '/www/gallery/';

    /**
     * @return array, информация о картинках в галерее (id, name)
     */
    public function getGallery()
    {
        $db = Db::init();
        $db->query('SELECT id, `name` FROM images');
        return $db->fetchAll();
    }

    /**
     * добавляет информацию (array $image) о картинке в бд
     * @param array $image
     * @return bool
     */
    public function add($image)
    {
        $db = Db::init();
        return $db->query("INSERT INTO images (`name`) VALUE (':name')", [':name' => $image]);
    }

    /**
     * загружает картинку в папку галереи
     * @param string $filename
     * @param string $destination
     * @return bool
     */
    public function upload($filename, $destination)
    {
        return move_uploaded_file($filename, "$this->path/$destination");
    }
}