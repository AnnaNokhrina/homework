<?php

namespace Anna\MyCity\Php\Classes;

class User
{
    /**
     * добавляет параметры в бд:
     * @param string $login
     * @param string $password
     * @return bool
     */
    public function add($login, $password)
    {
        $db = Db::init();
        $hash = password_hash($password, PASSWORD_DEFAULT);

        return $db->query("INSERT INTO `user` (login, password) VALUE (':login', ':password')",
            [':login' => $login, ':password' => $hash]);
    }


    /**
     * проверяет, соответствует ли хэш паролю
     * @param string $password
     * @param string $login
     * @return bool
     */
    public function passwordVerify($password, $login)
    {
        $db = Db::init();
        $db->query("SELECT password FROM `user` WHERE login=':login'", [':login' => $login]);
        $array = $db->fetch();
        $hash = $array['password'];

        return password_verify($password, $hash);
    }
}