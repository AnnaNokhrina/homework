<?php

namespace Anna\MyCity\Php\Classes;

class View
{
    protected $data = [];

    /**
     * передает переменную с именем $name и значением $value в шаблон
     * @param string $name
     * @param array $value
     */
    public function assign($name, $value)
    {
        $this->data[$name] = $value;
    }

    /**
     * отображает содержимое $template-шаблона
     * @param string $template
     */
    public function display($template)
    {
        extract($this->data);
        require '/www/View/' . $template . '.php';
    }
}