<?php

namespace Anna\MyCity\Php\Classes;

class Articles
{
    /**
     * @return array, статьи: заголовок, текст
     */
    public function getArticles()
    {
        $db = Db::init();
        $db->query('select `title`, `text` from `articles`');
        return $db->fetchAll();
    }

    /**
     * @param int $id
     * @return array, выбранная статья: заголовок, текст
     */
    public function getArticle($id)
    {
        $db = Db::init();
        $db->query("SELECT * FROM articles WHERE id = ':id'", [':id' => $id]);
        return $db->fetch();
    }

    /**
     * обновляет значения в заданных столбцах выбранной статьи в соответствие с $data
     * @param array $data
     * @return bool
     */
    public function edit($data)
    {
        $db = Db::init();
        $fields = $db->format($data);
        return $db->query("UPDATE articles SET `title` = ':title', `text` = ':text' WHERE id=:id", $fields);
    }
}