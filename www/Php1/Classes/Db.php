<?php

namespace Anna\MyCity\Php\Classes;

use PDO;
use PDOStatement;

class Db
{
    /** @var static */
    static $instance;

    /** @var PDO $dbh */
    protected $dbh;

    /** @var PDOStatement $sth */
    public $sth;

    /**
     * выполняет соединение между php и бд
     * Db constructor.
     */
    protected function __construct()
    {
        $config = require __DIR__ . '/../config_db.php';
        $this->dbh = new PDO($config['db']['dsn'], $config['db']['username'], $config['db']['password'], $config['db']['options']);
    }

    /**
     * @return Db
     */
    public static function init()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    /**
     * запускает подготовленный запрос $sql (с данными $data или без) на выполнение
     * @param string $sql
     * @param array $data
     * @return bool
     */
    public function query($sql, array $data = [])
    {
        foreach ($data as $key => $value) {
            $sql = str_replace($key, $value, $sql);
        }
        $this->sth = $this->dbh->prepare($sql);
        return $this->sth->execute();
    }

    /**
     * @return array, содержащий все строки результирующего набора выполненного запроса
     */
    public function fetchAll()
    {
        return $this->sth->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @return array, содержащий строку результирующего набора выполненного запроса
     */
    public function fetch()
    {
        return $this->sth->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @param $data
     * @return array
     */
    public function format($data)
    {
        $fields = [];
        foreach ($data as $key => $value) {
            $fields[':' . $key] = $value;
        }
        return $fields;
    }
}