<?php
/**
 * @var array $editArticle, статья: заголовок, текст
 */
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>project-9</title>
</head>
<body>

<form action="index.php" method="post">
    <input type="hidden" name="id" value="<?= $editArticle['id']; ?>">

    <input type="text" placeholder="заголовок" name="article" value="<?= $editArticle['article']; ?>">
    <input type="text" placeholder="текст" name="text" value="<?= $editArticle['text']; ?>">

    <button type="submit">Отправить</button>
</form>
</body>
</html>