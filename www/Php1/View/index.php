<?php
/**
 * @var array $articles, статьи: заголовок, текст
 */
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>project-9</title>
    <link href="/style.css" rel="stylesheet" type="text/css">
</head>
<body>

<header>
    <ul class="menu">
        <li><a href="/Php1/Control/index.php" class="navMenu">Главная</a></li>
        <li><a href="/Php1/Control/gallery.php" class="navMenu">Галерея</a></li>
        <li><a href="/Php1/Control/trainSchedule.php" class="navMenu">Расписание поездов</a></li>
        <?php if (isset($_SESSION['admin'])) { ?>
            <li><a href="/Php1/Control/logOut.php" class="navMenu">Выйти</a></li>
        <?php } else { ?>
            <li><a href="/Php1/View/View/login.php" class="navMenu">Войти</a></li>
        <?php } ?>
    </ul>
</header>


<div align="center">
    <h1>КРАСНОДАР</h1>
</div>
<hr>
<?php
foreach ($articles as $i => $article) { ?>
    <div align="center"><h2> <?php echo $articles[$i]['title']; ?> </h2></div>
    <?php if (isset($_SESSION['admin'])) { ?>
        <p align="center"><a href="/Php1/Control/index.php?id=<?= $i ?>" class="control">Редактировать</a></p>
    <?php } ?>
    <div> <?php echo $articles[$i]['text']; ?> </div> <?php
} ?>

</body>
</html>




