<?php
/**
 * @var array $image
 */
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>project 9</title>
    <link href="/style.css" rel="stylesheet" type="text/css">
</head>
<body>

<header>
    <ul class="menu">
        <li><a href="/Php1/Control/index.php" class="navMenu">Главная</a></li>
        <li><a href="/Php1/Control/gallery.php" class="navMenu">Галерея</a></li>
        <li><a href="/Php1/Control/trainSchedule.php" class="navMenu">Расписание поездов</a></li>
    </ul>
</header>

<img src="/<?= $image['name']; ?>" class="img">

</body>
</html>