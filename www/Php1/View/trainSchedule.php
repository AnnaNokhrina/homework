<?php
/**
 * @var array $trains, информация расписания поездов
 */
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>project-9</title>
    <link href="/style.css" rel="stylesheet" type="text/css">
</head>
<body>

<header>
    <ul class="menu">
        <li><a href="/Control/index.php" class="navMenu">Главная</a></li>
        <li><a href="/Control/gallery.php" class="navMenu">Галерея</a></li>
        <li><a href="/Control/trainSchedule.php" class="navMenu">Расписание поездов</a></li>
        <?php if (isset($_SESSION['admin'])) { ?>
            <li><a href="/Control/logOut.php" class="navMenu">Выйти</a></li>
        <?php } else { ?>
            <li><a href="/View/login.php" class="navMenu">Войти</a></li>
        <?php } ?>
    </ul>
</header>

<h1 align="center"> РАСПИСАНИЕ ПОЕЗДОВ</h1>
<hr>

<table border="2" width="100%" cellpadding="5">

    <tr align="center">
        <th width="2%">№</th>
        <th width="14%">Тип поезда</th>
        <th width="14%">Маршрут</th>
        <th width="14%">Отправление</th>
        <th width="14%">Прибытие</th>
        <th width="14%">В пути</th>
        <th width="14%">Цена (руб)</th>
        <?php if (isset($_SESSION['admin'])) { ?>
            <th>Управление</th>
        <?php } ?>
    </tr>

    <?php foreach ($trains as $i => $train) { ?>
        <tr>
        <?php foreach ($train as $key => $value) { ?>
            <td align="center"> <?= $value; ?> </td>
        <?php } ?>
        <?php if (isset($_SESSION['admin'])) { ?>
            <td>
                <a href="/Control/trainSchedule.php?action=edit&id=<?= $train['id'] ?>"
                   class="control">Редактировать</a>
                <a href="/Control/trainSchedule.php?action=delete&id=<?= $train['id'] ?>" class="control">Удалить</a>
            </td>
            </tr>
        <?php }
    } ?>
</table>

<?php if (isset($_SESSION['admin'])) { ?>

    <hr>
    <p align="center">ДОБАВИТЬ РАСПИСАНИЕ </p>
    <hr>
    <form action="trainSchedule.php" method="post">
        <input type="text" placeholder="тип поезда" name="train">
        <input type="text" placeholder="маршрут" name="route">
        <input type="time" placeholder="время отправления" name="departure">
        <input type="time" placeholder="время прибытия" name="arrival">
        <input type="text" placeholder="время в пути" name="time">
        <input type="text" placeholder="цена(руб)" name="price">
        <button type="submit">Добавить</button>
    </form>

<?php } ?>

</body>
</html>