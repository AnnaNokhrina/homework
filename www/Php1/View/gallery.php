<?php
/**
 * @var array $images, информация о картинках  (id, name)
 */
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>project 9</title>
    <link href="/style.css" rel="stylesheet" type="text/css">
</head>
<body>

<header>
    <ul class="menu">
        <li><a href="/Php1/Control/index.php" class="navMenu">Главная</a></li>
        <li><a href="/Php1/Control/gallery.php" class="navMenu">Галерея</a></li>
        <li><a href="/Php1/Control/trainSchedule.php" class="navMenu">Расписание поездов</a></li>
        <?php if (isset($_SESSION['admin'])) { ?>
            <li><a href="/Php1/Control/logOut.php" class="navMenu">Выйти</a></li>
        <?php } else { ?>
            <li><a href="/Php1/View/View/login.php" class="navMenu">Войти</a></li>
        <?php } ?>
    </ul>
</header>

<h1 align="center">ГАЛЕРЕЯ</h1>
<hr>

<?php
if (isset($_SESSION['admin'])) { ?>

    <form action="gallery.php" method="post" enctype="multipart/form-data">
        <input type="file" name="image">
        <input type="submit" value="Загрузить">
    </form>
    <hr>

<?php } ?>

<div class="blockimage">
    <?php foreach ($images as $i => $image) { ?>
        <div class="arr"><a href="/Php1/Control/image.php?id= <?= $i; ?>"><img src="/<?= $images[$i]['name']; ?>"></a></div>
    <?php } ?>
</div>

</body>
</html>