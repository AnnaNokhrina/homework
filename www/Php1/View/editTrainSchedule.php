<?php
/**
 * @var array $train, информация расписания поезда
 */
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>project-9</title>
</head>
<body>


<form action="trainSchedule.php" method="post">
    <input type="hidden" name="id" value="<?= $train['id']; ?>">
    <input type="text" placeholder="тип поезда" name="train" value="<?= $train['train']; ?>">
    <input type="text" placeholder="маршрут" name="route" value="<?= $train['route']; ?>">
    <input type="time" placeholder="время отправления" name="departure" value="<?= $train['departure']; ?>">
    <input type="time" placeholder="время прибытия" name="arrival" value="<?= $train['arrival']; ?>">
    <input type="text" placeholder="время в пути" name="time" value="<?= $train['time']; ?>">
    <input type="text" placeholder="цена(руб)" name="price" value="<?= $train['price']; ?>">
    <button type="submit">Отправить</button>
</form>
</body>
</html>