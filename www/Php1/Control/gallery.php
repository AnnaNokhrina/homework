<?php
session_start();
require_once __DIR__ . '/../autoload.php';
ini_set('display_errors', 1);

$gallery = new Anna\MyCity\Php\Classes\Gallery();
$view = new Anna\MyCity\Php\Classes\View();

$typeFile = ['image/jpeg', 'image/jpg', 'image/png'];

if (isset($_SESSION['admin']) && isset($_FILES['image']) && 0 == $_FILES['image']['error']) {
    if (in_array($_FILES['image']['type'], $typeFile) && $gallery->upload($_FILES['image']['tmp_name'], $_FILES['image']['name'])) {
        $gallery->add($_FILES['image']['name']);
        return header("Refresh:0");
    }
}

$view->assign('images', $gallery->getGallery());
$view->display('gallery');