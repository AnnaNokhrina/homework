<?php

require_once __DIR__ . '/../autoload.php';
ini_set('display_errors', 1);

$gallery = new Anna\MyCity\Php\Classes\Gallery();
$view = new  Anna\MyCity\Php\Classes\View();

$image = $gallery->getGallery()[(int)$_GET['id']];

$view->assign('image', $image);
$view->display('image');
