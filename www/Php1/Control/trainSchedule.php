<?php
session_start();

ini_set('display_errors', 1);

require_once __DIR__ . '/../autoload.php';

$view = new Anna\MyCity\Php\Classes\View();
$trainSchedule = new Anna\MyCity\Php\Classes\TrainSchedule();


if (isset($_SESSION['admin'])) {
    if (isset($_GET['action']) && $_GET['action'] == 'edit') {
        $view->assign('train', $trainSchedule->getTrain($_GET['id']));
        $view->display('editTrainSchedule');
        exit;
    }
    if (isset($_GET['action']) && $_GET['action'] == 'delete') {
        $trainSchedule->delete($_GET['id']);
        header("Location: /Control/trainSchedule.php");
    }

    if (!empty($_POST) && (!empty($_POST['train']) && !empty($_POST['route']) && !empty($_POST['departure']) &&
            !empty($_POST['arrival']) && !empty($_POST['time']) && !empty($_POST['price']))) {
        if (isset($_POST['id'])) {
            $trainSchedule->edit($_POST);
        } else {
            $trainSchedule->add($_POST);
        }
        header("Refresh:0");
    }
}

$view->assign('trains', $trainSchedule->getTrains());
$view->display('trainSchedule');