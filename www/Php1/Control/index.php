<?php
session_start();

require_once __DIR__ . '/../autoload.php';
ini_set('display_errors', 1);

$articles = new Anna\MyCity\Php\Classes\Articles();
$view = new Anna\MyCity\Php\Classes\View();

if (isset($_SESSION['admin'])) {
    if (isset($_GET['id'])) {
        $view->assign('editArticle', $articles->getArticle($_GET['id']));
        $view->display('editArticle');
        exit;
    }
    if (isset($_POST['id'])) {
        $articles->edit($_POST);
    }
}

$view->assign('articles', $articles->getArticles());
$view->display('index');