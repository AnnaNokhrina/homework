<?php
session_start();

require_once __DIR__ . '/../autoload.php';
ini_set('display_errors', 1);

$user = new Anna\MyCity\Php\Classes\User();

header("Refresh:0");
if ($user->passwordVerify($_POST['password'], $_POST['login'])) {
    $_SESSION['admin'] = $_POST['login'];
    header('Location:/Control/index.php');
}